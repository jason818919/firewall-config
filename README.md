# **Introduction**
------
This bash script is an automatic security setting for Debian-8.2.0-amd64 operating system. It is designed to configure the daemon (service) and firewall. In other words, it closes the redundant daemons, establishes the iptables
for both IPv4 and IPv6, modifies the sshd configuration and change the sshd port number to another safer one. All these setting are done in this one script. There is no need to configure. If you want your sshd port number be different,
just modify the iptables for both IPv4 and IPv6, and also the sshd configuration file.

# **Important**
------
If you need a sftp user, you should creat a user with safe password and check its home folder permission is 755. Next, uncomment the last part of sshd_config.test.
Add the new match user name if multiple (testuser1,testuser2). For the accessibility, root should make a directory which belonging to the user and set the permission to 700.
Then, open the /etc/passwd and change its login shell to /bin/false. Finally, restart the sshd by the following command.
```
$ service sshd restart
```

# **Usage**
------
This script should be executed by root.
To configure the security setting automatically, just install this script.
```
$ ./install
```
If the computer does not do the reboot, make sure your permission for execution. Enable the execution of this file.
```
$ chmod +x install
```
Or just use the bash command.
```
$ bash install
```
After the installation, we should check the iptable with the following command.
```
$ iptables -L
$ ip6tables -L
```

# **Iptables**
------
The default iptables only allow the following input port.

1. http 		80
2. https 		443
3. sshd 		23145
4. costum usage 	54132, 54133, 54134
5. DNS			53
6. All related packages which are sent by our own computer.
7. SMTP			25
8. SMTPS		465
9. IMAPS		993
10. NTP			123
11. VPN	(optional)	1194
12. Samba		139, (137, 138, 445 are optional)
13. NVC			5901, 5902

Output are all allowed.

Forward are all dropped.

# **Author**

Jason Chen
